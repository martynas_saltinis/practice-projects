public class Printer {
    public String printError(String controlString) {
        if(controlString.length() == 0)
            throw new EmptyControlStringException();
        String controlValidationPattern = "^[a-z]+$";
        if(!controlString.matches(controlValidationPattern))
            throw new IllegalSymbolsInControlString();
        return String.format("%d/%d", detectProblems(controlString), controlString.length());
    }

    private int detectProblems(String controlString) {
        int detectedProblems = 0;
        for(int i = 0; i < controlString.length(); i++){
            char symbol = controlString.charAt(i);
            if(String.format("%s", symbol).matches("[^a-m]"))
                detectedProblems++;
        }
        return detectedProblems;
    }

    class EmptyControlStringException extends RuntimeException {
    }

    class IllegalSymbolsInControlString extends RuntimeException{
    }
}
