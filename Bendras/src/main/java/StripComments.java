import java.util.Arrays;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class StripComments {
    public String stripComments(String text, String[] commentSymbols) {
        validateInput(text, commentSymbols);
        if (commentSymbols.length == 0)
            return text;
        String[] textLines = text.split("\n");
        return getStrippedText(commentSymbols, textLines);
    }

    private String getStrippedText(String[] commentSymbols, String[] textLines) {
        StringBuilder strippedText = new StringBuilder();
        for(int i = 0; i < textLines.length; i++){
            strippedText.append(getStrippedLine(textLines[i], commentSymbols));
            if(i < textLines.length - 1){
                strippedText.append("\n");
            }
        }
        return strippedText.toString();
    }

    private String getStrippedLine(String text, String[] commentSymbols) {
        String strippedText = text;
        for(String commentSymbol : commentSymbols){
            int symbolIdInText = strippedText.indexOf(commentSymbol);
            if(symbolIdInText != -1){
                strippedText = strippedText.substring(0, symbolIdInText);
            }
        }
        return rightTrim(strippedText);
    }

    private void validateInput(String text, String[] commentSymbols) {
        requireNonNull(text);
        requireNonNull(commentSymbols);
        if(containsNull(commentSymbols)){
            throw new NullPointerException();
        }
    }

    private boolean containsNull(String[] commentSymbols) {
        return Arrays.stream(commentSymbols).anyMatch(Objects::isNull);
    }

    private String rightTrim(String text) {
        int i = text.length()-1;
        while (i >= 0 && Character.isWhitespace(text.charAt(i))) {
            i--;
        }
        return text.substring(0,i+1);
    }
}
