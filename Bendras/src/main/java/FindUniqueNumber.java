import java.util.HashMap;
import java.util.Map;

public class FindUniqueNumber {

    public Integer findUnique(int array[]) {
        if (array == null) {
            throw new NullPointerException();
        }

        HashMap<Integer, Integer> frequencies = getNumberFrequencyMap(array);

        return getNumberWithOneFrequency(frequencies);
    }

    private Integer getNumberWithOneFrequency(HashMap<Integer, Integer> frequencies) {
        for (Map.Entry<Integer, Integer> entry : frequencies.entrySet()) {
            if (entry.getValue() == 1) {
                return entry.getKey();
            }
        }
        return null;
    }


    private HashMap<Integer, Integer> getNumberFrequencyMap(int[] array) {
        HashMap<Integer, Integer> frequencies = new HashMap<>();
        for (int element : array) {
            if (!frequencies.containsKey(element)) {
                frequencies.put(element, 1);
            } else {
                int currentFrequency = frequencies.get(element);
                frequencies.put(element, ++currentFrequency);
            }
        }
        return frequencies;
    }
}
