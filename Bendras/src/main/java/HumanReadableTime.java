/*Write a function, which takes a non-negative integer (seconds) as input and returns the time in a human-readable format (HH:MM:SS)

        HH = hours, padded to 2 digits, range: 00 - 99
        MM = minutes, padded to 2 digits, range: 00 - 59
        SS = seconds, padded to 2 digits, range: 00 - 59

        The maximum time never exceeds 359999 (99:59:59)

        You can find some examples in the test fixtures.*/


public class HumanReadableTime {

    public String makeReadable(int seconds) throws OnlyPositiveNumbersAllowed {
        if(seconds < 0){
            throw new OnlyPositiveNumbersAllowed();
        }

        String formattedHours = addPaddingToTime(seconds / 3600);
        String formattedMinutes = addPaddingToTime((seconds % 3600) / 60);
        String formattedSeconds = addPaddingToTime(seconds % 60);

        return formattedHours + ":" + formattedMinutes + ":" + formattedSeconds;
    }

    private String addPaddingToTime(int number) {
        if(number < 10) {
            return "0" + number;
        } else {
            return Integer.toString(number);
        }
    }

    class OnlyPositiveNumbersAllowed extends Throwable {
    }
}
