import java.util.Arrays;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class Finder {
    private static final int START_X = 0;
    private static final int START_Y = 0;
    private static final char WALK_TILE = '.';
    private static final char WALL_TILE = 'W';


    public int pathFinder(String maze) throws IllegalSymbolsInInput, InvalidMatrixDimensions {
        requireNonNull(maze);
        verifyInputSymbolValidity(maze);
        char[][] mazeMatrix = getMazeMatrix(maze);
        if (!startAndEndExist(mazeMatrix)) {
            return -1;
        }


        return getShortestPathLength(0, 0, mazeMatrix, 0);
    }

    private int getShortestPathLength(int row, int column, char[][] mazeMatrix, int currentLength) {
        if(mazeMatrix[row][column] != WALK_TILE){
            return -1;
        }

        currentLength++;
        mazeMatrix[row][column] = WALL_TILE;
        int shortest = -1;
        /*if(row + 1 < mazeMatrix.length){
            int length = getShortestPathLength(row+1, column, mazeMatrix, currentLength);
            if(shortestFromHere > length && length != -1)
                shortestFromHere = length;
        }*/
        /*if(column + 1 < mazeMatrix[0].length){
            int length = getShortestPathLength(row, column+1, mazeMatrix, currentLength);
            if(length != -1){
                if(shortest == -1){
                    if()
                }
            }
        }*/
        /*if(column - 1 >= 0){
            int length = getShortestPathLength(row, column-1, mazeMatrix, currentLength);
            if(shortestFromHere > length && length != -1)
                shortestFromHere = length;
        }
        if(row - 1 >= 0){
            int length = getShortestPathLength(row-1, column, mazeMatrix, currentLength);
            if(shortestFromHere > length && length != -1)
                shortestFromHere = length;
        }*/
        return shortest;
    }

    private char[][] getMazeMatrix(String maze) throws InvalidMatrixDimensions {
        String[] rows = maze.split("\n");
        verifyMazeDimensions(rows);
        char[][] matrix = new char[rows.length][rows[0].length()];
        for (int row = 0; row < rows.length; row++) {
            char[] rowSymbols = rows[row].toCharArray();
            for (int column = 0; column < rows[row].length(); column++) {
                matrix[row][column] = rowSymbols[column];
            }
        }
        return matrix;
    }

    private void verifyMazeDimensions(String[] rows) throws InvalidMatrixDimensions {
        int rowLength = rows[0].length();
        for(String row : rows){
            if(rowLength != row.length())
                throw new InvalidMatrixDimensions();
        }
    }

    private boolean startAndEndExist(char[][] maze) {
        char start = maze[START_X][START_Y];
        char end = maze[maze.length - 1][maze[maze.length - 1].length - 1];

        return start == WALK_TILE && end == WALK_TILE;
    }

    private void verifyInputSymbolValidity(String input) throws IllegalSymbolsInInput {
        List<Character> allowedSymbols = Arrays.asList(WALK_TILE, WALL_TILE, '\n');
        char[] symbols = input.toUpperCase().toCharArray();
        for (char symbol : symbols) {
            if (!allowedSymbols.contains(symbol))
                throw new IllegalSymbolsInInput();
        }
    }

    class IllegalSymbolsInInput extends Throwable {
    }

    class InvalidMatrixDimensions extends Throwable {
    }
}
