import java.util.ArrayList;
import java.util.List;

/*
You have to create a function that takes a positive integer number and returns the biggest number formed by the same digits:

        12 ==> 21
        513 ==> 531
        2017 ==> 7210

        If no bigger number can be composed using those digits, return -1:

        9 ==> -1
        111 ==> -1
        531 ==> -1

*/


public class BiggestNumber {
    public long nextBiggerNumber(long n) {
        if(n <= 0)
            throw new OnlyPositiveWholeNumbersAllowed();
        if(n / 10 == 0){
            return n;
        }
        List<Long> digits = getDigits(n);
        List<Long> biggestNumberDigits = arrangeBiggestNumberDigits(digits);
        return formNumberFromDigits(biggestNumberDigits);
    }

    private long formNumberFromDigits(List<Long> numberDigits) {
        List<Long> numberDigitsCopy = new ArrayList<>(numberDigits);
        long number = 0;
        int digitId = 1;
        while(numberDigitsCopy.size() > 0){
            number = number * (long)Math.pow(10, digitId) + numberDigitsCopy.get(0);
            numberDigitsCopy.remove(0);
        }
        return number;
    }

    private List<Long> arrangeBiggestNumberDigits(List<Long> digits) {
        List<Long> digitsCopy = new ArrayList<>(digits);
        List<Long> biggestNumberDigits = new ArrayList<>();
        while(digitsCopy.size() > 0){
            long biggestDigit = getBiggestDigitFromTargetListThatIsNotInOtherList(digitsCopy,biggestNumberDigits);
            biggestNumberDigits.add(biggestDigit);
            digitsCopy.remove(biggestDigit);
        }
        return biggestNumberDigits;
    }

    private Long getBiggestDigitFromTargetListThatIsNotInOtherList(List<Long> target, List<Long> other) {
        long biggestDigit = Long.MIN_VALUE;
        for(int i = 0; i < target.size(); i++){
                if(!other.contains(target.get(i))){
                    if(target.get(i) > biggestDigit)
                        biggestDigit = target.get(i);
                }
        }
        return biggestDigit;
    }

    private List<Long> getDigits(long n) {
        List<Long> digits = new ArrayList<>();
        while (n> 0){
            long digit = n % 10;
            n = n / 10;
            digits.add(digit);
        }
        return digits;
    }

    class OnlyPositiveWholeNumbersAllowed extends RuntimeException {
    }
}
