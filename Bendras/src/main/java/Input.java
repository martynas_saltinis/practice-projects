import java.util.ArrayList;
import java.util.List;

public class Input {
    private List<Integer> pressedDirections;

    public Input() {
        this.pressedDirections = new ArrayList<>();
    }

    public void press(int direction) {
        if(!pressedDirections.contains(direction)){
            pressedDirections.add(direction);
        }
    }



    public boolean getState(int direction){
        return pressedDirections.contains(direction);
    }

    public void clear(){
        pressedDirections.clear();
    }

    public void release(int direction) {
        pressedDirections.remove(Integer.valueOf(direction));
    }
}
