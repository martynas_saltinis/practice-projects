/*Write a function that determines whether a string is a valid guess in a Boggle board, as per the rules of Boggle. A Boggle board is a 2D array of individual characters, e.g.:

        [ ['I','L','A','W'],
        ['B','N','G','E'],
        ['I','U','A','O'],
        ['A','S','R','L'] ]

        Valid guesses are strings which can be formed by connecting adjacent cells (horizontally, vertically, or diagonally) without re-using any previously used cells.

        For example, in the above board "BINGO", "LINGO", and "ILNBIA" would all be valid guesses, while "BUNGIE", "BINS", and "SINUS" would not.

        Your function should take two arguments (a 2D array and a string) and return true or false depending on whether the string is found in the array as per Boggle rules.*/

public class Boggle {

    private String wordToFind;
    private static final String WORD_LETTERS_MATCH = "^[A-Z]*$";

    public boolean check(final char[][] board, final String word) {
        if (board == null || word == null)
            throw new InvalidDataException();
        wordToFind = word.trim().toUpperCase();
        validateInput(board, wordToFind);
        if (wordToFind.length() == 0)
            return true;
        for (int i = 0; i < board.length; i++)
            for (int j = 0; j < board[i].length; j++)
                if (isLetterStartOfWord(board[i][j]) && canFindFullWord(i, j, board))
                    return true;
        return false;
    }

    private boolean canFindFullWord(int i, int j, char[][] board) {
        return findWord(i, j, board, new boolean[board.length][board.length], "");
    }

    private boolean isLetterStartOfWord(char boardLetter) {
        return wordToFind.charAt(0) == boardLetter;
    }

    private boolean findWord(int x, int y, char[][] board, boolean[][] usedLetters, String word) {
        usedLetters[x][y] = true;
        String wordCopy = word + board[x][y];
        if (wordFound(wordCopy))
            return true;
        if (x - 1 >= 0 && !usedLetters[x - 1][y])
            if (findWord(x - 1, y, board, usedLetters, wordCopy))
                return true;
        if (x - 1 >= 0 && y + 1 < board.length && !usedLetters[x - 1][y + 1])
            if (findWord(x - 1, y + 1, board, usedLetters, wordCopy))
                return true;
        if (y + 1 < board.length && !usedLetters[x][y + 1])
            if (findWord(x, y + 1, board, usedLetters, wordCopy))
                return true;
        if (x + 1 < board.length && y + 1 < board.length && !usedLetters[x + 1][y + 1])
            if (findWord(x + 1, y + 1, board, usedLetters, wordCopy))
                return true;
        if (x + 1 < board.length && !usedLetters[x + 1][y])
            if (findWord(x + 1, y, board, usedLetters, wordCopy))
                return true;
        if (x + 1 < board.length && y - 1 >= 0 && !usedLetters[x + 1][y - 1])
            if (findWord(x + 1, y - 1, board, usedLetters, wordCopy))
                return true;
        if (y - 1 >= 0 && !usedLetters[x][y - 1])
            if (findWord(x, y - 1, board, usedLetters, wordCopy))
                return true;
        if (x - 1 >= 0 && y - 1 >= 0 && !usedLetters[x - 1][y - 1])
            if (findWord(x - 1, y - 1, board, usedLetters, wordCopy))
                return true;
        usedLetters[x][y] = false;
        return false;
    }

    private boolean wordFound(String wordCopy) {
        return wordToFind.equalsIgnoreCase(wordCopy);
    }

    private void validateInput(char[][] board, String word) {
        if (!isAWord(word))
            throw new OnlyLettersInWord();
        validateBoard(board);
    }

    private void validateBoard(char[][] board) {
        for (char[] chars : board) {
            if (chars.length != board.length)
                throw new BoardDimensionsShouldBeNxN();
            if (!isAWord(new String(chars)))
                throw new OnlyLettersInBoard();
        }
    }

    private boolean isAWord(String word) {
        return word.matches(WORD_LETTERS_MATCH);
    }

    class BoardDimensionsShouldBeNxN extends RuntimeException {
    }

    class OnlyLettersInBoard extends RuntimeException {
    }

    class OnlyLettersInWord extends RuntimeException {
    }

    class InvalidDataException extends RuntimeException {
    }
}
