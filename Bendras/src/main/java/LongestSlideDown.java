/*
###Lyrics... Pyramids are amazing! Both in architectural and mathematical sense. If you have a computer, you can mess with pyramids even if you are not in Egypt at the time. For example, let's consider the following problem. Imagine that you have a plane pyramid built of numbers, like this one here:

   /3/
  \7\ 4
 2 \4\ 6
8 5 \9\ 3

Here comes the task...

Let's say that the 'slide down' is a sum of consecutive numbers from the top to the bottom of the pyramid. As you can see, the longest 'slide down' is 3 + 7 + 4 + 9 = 23

Your task is to write a function longestSlideDown (in ruby: longest_slide_down) that takes a pyramid representation as argument and returns its' longest 'slide down'. For example,

longestSlideDown [[3], [7, 4], [2, 4, 6], [8, 5, 9, 3]]
// => 23

###By the way... My tests include some extraordinarily high pyramides so as you can guess, brute-force method is a bad idea unless you have a few centuries to waste. You must come up with something more clever than that.
 */

public class LongestSlideDown {

    public int longestSlideDown(int[][] pyramid) throws PyramidMustHaveLevels, PyramidMustNotBeNull {
        checkForCorrectPyramid(pyramid);

        int[][] lengthsTable = new int[pyramid.length][pyramid[pyramid.length-1].length];
        lengthsTable = fillBottomLengths(lengthsTable, pyramid);

        for(int i = pyramid.length - 2; i >= 0; i--){
            for(int j = 0; j <= i; j++){
                int left = lengthsTable[i + 1][j];
                int right = lengthsTable[i + 1][j + 1];
                lengthsTable[i][j] = pyramid[i][j] + Math.max(left, right);
            }
        }

        return getLongestFromTop(lengthsTable[0]);
    }

    private int getLongestFromTop(int[] ints) {
        int longest = 0;
        for(int i = 0; i < ints.length; i++){
            if(ints[i] > longest)
                longest = ints[i];
        }
        return longest;
    }

    private int[][] fillBottomLengths(int[][] dynamicTable, int[][] pyramid) {
        int lastRowSize = pyramid[pyramid.length - 1].length;
        for(int i = 0; i < lastRowSize; i++){
            dynamicTable[pyramid.length-1][i] = pyramid[pyramid.length-1][i];
        }
        return dynamicTable;
    }

    private void checkForCorrectPyramid(int[][] pyramid) throws PyramidMustNotBeNull, PyramidMustHaveLevels {
        if(pyramid == null){
            throw new PyramidMustNotBeNull();
        }
        if(pyramid.length == 0){
            throw new PyramidMustHaveLevels();
        }
    }

    class PyramidMustHaveLevels extends Throwable {

    }

    class PyramidMustNotBeNull extends Throwable {

    }
}
