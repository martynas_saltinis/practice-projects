public class Tile {
    final int x;
    final int y;

    public Tile(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("(%d,%d)", x, y);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + x + y;
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Tile)) {
            return false;
        }
        Tile tile = (Tile) other;
        return tile.x == x && tile.y == y;
    }
}
