import com.sun.deploy.util.ArrayUtil;

import java.util.*;

import static java.util.Objects.requireNonNull;

public class SecretDetective {

    public String recoverSecret(char[][] triplets) throws DuplicatesNotAllowed, TripletsMustBeOfSizeThree {
        requireNonNull(triplets);
        if(triplets.length == 1){
            return new String(triplets[0]);
        }
        HashMap<Character, List<Character>> symbolsAfterSymbol = generateSymbolsAfterSymbolMap(triplets);

        Character lastLetter = requireNonNull(findKeyWithEmptyList(symbolsAfterSymbol));
        String word = new StringBuilder(requireNonNull(findWord(symbolsAfterSymbol, lastLetter, "")))
                .reverse()
                .toString();
        return word;
    }

    private String findWord(HashMap<Character, List<Character>> symbolsAfterSymbol, Character letter, String word) {
        if(word.length() == symbolsAfterSymbol.keySet().size()){
            return word;
        }
        word += letter;
        for (Map.Entry<Character, List<Character>> entry : symbolsAfterSymbol.entrySet()) {
            List<Character> value = entry.getValue();
            if(value.contains(letter)){
                word = findWord(symbolsAfterSymbol, entry.getKey(), word);
            }
        }
        return word;
    }

    private Character findKeyWithEmptyList(HashMap<Character, List<Character>> symbolsAfterSymbol) {
        Character keyWithEmptyList = null;
        for (Map.Entry<Character, List<Character>> entry : symbolsAfterSymbol.entrySet()) {
            List<Character> value = entry.getValue();
            if(value.size() == 0){
                keyWithEmptyList = entry.getKey();
            }
        }
        return keyWithEmptyList;
    }

    private HashMap<Character, List<Character>>  generateSymbolsAfterSymbolMap(char[][] triplets) throws DuplicatesNotAllowed, TripletsMustBeOfSizeThree {
        HashMap<Character, List<Character>> symbolsAfterSymbol = new HashMap<>();
        for(char[] triplet : triplets){
            if(triplet.length != 3){
                throw new TripletsMustBeOfSizeThree();
            }
            HashSet<Character> symbolsInTriplet = new HashSet<>();
            for(int symbolId = 0; symbolId < triplet.length; symbolId++){
                if(symbolsInTriplet.contains(triplet[symbolId])){
                    throw new DuplicatesNotAllowed();
                } else {
                    symbolsInTriplet.add(triplet[symbolId]);
                    char[] afterSymbols = Arrays.copyOfRange(triplet, symbolId + 1, triplet.length);
                    List<Character> afterSymbolList = createCharacterListFromArray(afterSymbols);
                    if(symbolsAfterSymbol.containsKey(triplet[symbolId])){
                        List<Character> existingList = symbolsAfterSymbol.get(triplet[symbolId]);
                        List<Character> merged = mergeTwoListsWithoutDuplicates(existingList, afterSymbolList);
                        symbolsAfterSymbol.put(triplet[symbolId], merged);
                    }else{
                        symbolsAfterSymbol.put(triplet[symbolId], afterSymbolList);
                    }
                }
            }
        }
        return symbolsAfterSymbol;
    }

    private List<Character> mergeTwoListsWithoutDuplicates(List<Character> one, List<Character> two) {
        List<Character> twoCopy = new ArrayList<>(two);
        twoCopy.removeAll(one);
        one.addAll(twoCopy);
        return one;
    }

    private List<Character> createCharacterListFromArray(char[] charArray) {
        List<Character> characterList = new ArrayList<>();
        for (char c : charArray) {
            characterList.add(c);
        }

        return characterList;
    }

    class DuplicatesNotAllowed extends Throwable {
    }

    class TripletsMustBeOfSizeThree extends Throwable {
    }
}
