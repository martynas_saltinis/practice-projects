import javafx.util.converter.BigIntegerStringConverter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class GreenNumbers {
    public BigInteger get(int n) {
        if(n < 1)
            throw new InputLowerThanOneException();
        BigInteger number = BigInteger.ONE;
        List<BigInteger> greenNumbers = new ArrayList<>();
        while(greenNumbers.size() < n){
            int digitsLength = number.toString().length();
            if(getLastNDigits(number.pow(2), digitsLength).equals(number)){
                greenNumbers.add(number);
            }
            number = number.add(BigInteger.ONE);
        }
        return greenNumbers.get(n - 1);
    }

    private BigInteger getLastNDigits(BigInteger number, int nLastDigits) {
        int numberDigitsLength = number.toString().length();
        String lastDigits = number.toString().substring(numberDigitsLength - nLastDigits);
        return new BigInteger(lastDigits);
    }

    class InputLowerThanOneException extends RuntimeException{
    }
}
