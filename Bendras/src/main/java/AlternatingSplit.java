/*
For building the encrypted string:
        Take every 2nd char from the string, then the other chars, that are not every 2nd char, and concat them as new String.
        Do this n times!

        Examples:

        "This is a test!", 1 -> "hsi  etTi sats!"
        "This is a test!", 2 -> "hsi  etTi sats!" -> "s eT ashi tist!"

        Write two methods:

        String encrypt(final String text, final int n)
        String decrypt(final String encryptedText, final int n)

        For both methods:
        If the input-string is null or empty return exactly this value!
        If n is <= 0 then return the input text.

*/

public class AlternatingSplit {
    public String encrypt(final String text, final int n) {
        if(text==null || text.length() == 0){
            return text;
        }

        StringBuilder encryption = new StringBuilder(text);
        for(int i = 0; i < n; i++){
            encryption = formEncryption(encryption.toString());
        }

        return encryption.toString();
    }

    private StringBuilder formEncryption(String text) {
        StringBuilder formedText = new StringBuilder();
        StringBuilder oddSymbols = new StringBuilder();

        for(int i = 0; i < text.length(); i++){
            if((i+1)%2==0){
                formedText.append(text.charAt(i));
            }else{
                oddSymbols.append(text.charAt(i));
            }
        }

        return formedText.append(oddSymbols.toString());
    }

    private StringBuilder formDecryption(String text) {
        String firstHalf = text.substring(0, text.length() / 2);
        String secondHalf = text.substring((text.length() / 2));

        return combineTextFromOddsAndEvens(secondHalf, firstHalf);
    }

    private StringBuilder combineTextFromOddsAndEvens(String oddSymbols, String evenSymbols) {
        StringBuilder combinedText = new StringBuilder();
        int combinationsCount = 0;
        int symbolsCount = 0;
        boolean allCombined = false;
        boolean isOdd = true;

        while(!allCombined){
            if(isOdd){
                combinedText.append(oddSymbols.charAt(combinationsCount));
                isOdd = false;
            }else{
                combinedText.append(evenSymbols.charAt(combinationsCount++));
                isOdd = true;
            }
            symbolsCount++;
            if(symbolsCount == oddSymbols.length() + evenSymbols.length()){
                allCombined = true;
            }
        }

        return combinedText;
    }

    public String decrypt(final String encryptedText, final int n) {
        if(encryptedText==null || encryptedText.length() == 0){
            return encryptedText;
        }

        StringBuilder decryption = new StringBuilder(encryptedText);
        for(int i = 0; i < n; i++){
            decryption = formDecryption(decryption.toString());
        }

        return decryption.toString();
    }
}
