import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.util.Objects.requireNonNull;

/*
Alright, detective, one of our colleagues successfully observed our target person, Robby the robber. We followed him to a secret warehouse, where we assume to find all the stolen stuff. The door to this warehouse is secured by an electronic combination lock. Unfortunately our spy isn't sure about the PIN he saw, when Robby entered it.

        The keypad has the following layout:

        ┌───┬───┬───┐
        │ 1 │ 2 │ 3 │
        ├───┼───┼───┤
        │ 4 │ 5 │ 6 │
        ├───┼───┼───┤
        │ 7 │ 8 │ 9 │
        └───┼───┼───┘
        │ 0 │
        └───┘

        He noted the PIN 1357, but he also said, it is possible that each of the digits he saw could actually be another adjacent digit (horizontally or vertically, but not diagonally). E.g. instead of the 1 it could also be the 2 or 4. And instead of the 5 it could also be the 2, 4, 6 or 8.

        He also mentioned, he knows this kind of locks. You can enter an unlimited amount of wrong PINs, they never finally lock the system or sound the alarm. That's why we can try out all possible (*) variations.

        * possible in sense of: the observed PIN itself and all variations considering the adjacent digits

        Can you help us to find all those variations? It would be nice to have a function, that returns an array (or a list in Java and C#) of all variations for an observed PIN with a length of 1 to 8 digits. We could name the function getPINs (get_pins in python, GetPINs in C#). But please note that all PINs, the observed one and also the results, must be strings, because of potentially leading '0's. We already prepared some test cases for you.

        Detective, we count on you!
*/


public class ObservedPin {

    HashMap<String, String[]> neighbors = new HashMap<String, String[]>() {{
        put("1", new String[]{"1", "2", "4"});
        put("2", new String[]{"2", "1", "3", "5"});
        put("3", new String[]{"3", "2", "6"});
        put("4", new String[]{"4", "1", "5", "7"});
        put("5", new String[]{"5", "2", "4", "6", "8"});
        put("6", new String[]{"6", "3", "5", "9"});
        put("7", new String[]{"7", "4", "8"});
        put("8", new String[]{"8", "0", "5", "7"});
        put("9", new String[]{"9", "6", "8"});
        put("0", new String[]{"0", "8"});
    }};

    public List<String> getPINs(String observed) throws OnlyNumbersAllowed {
        detectIllegalInput(observed);

        String[] digits = getDigitsFromString(observed);

        List<String> pins = new ArrayList<>();
        pins = createFirstDigitVariations(pins, digits[0]);
        for (int i = 1; i < digits.length; i++) {
            pins = addCombinationsForDigit(pins, digits[i]);
        }
        return pins;
    }

    private List<String> createFirstDigitVariations(List<String> pins, String digit) {
        List<String> newPins = (ArrayList<String>) ((ArrayList<String>) pins).clone();
        for (String neighbor : neighbors.get(digit)) {
            newPins.add(neighbor);
        }
        return newPins;
    }

    private List<String> addCombinationsForDigit(List<String> pins, String digit) {
        List<String> newPins = new ArrayList<>();
        for (String pin : pins) {
            for (String neighbor : neighbors.get(digit)) {
                String newPin = pin + neighbor;
                newPins.add(newPin);
            }
        }
        return newPins;
    }

    private String[] getDigitsFromString(String observed) {
        return observed.split("");
    }

    private void detectIllegalInput(String observed) throws OnlyNumbersAllowed {
        requireNonNull(observed);
        String ONLY_NUMBER_ATLEAST_ONE = "^[0-9]+$";
        if (!observed.matches(ONLY_NUMBER_ATLEAST_ONE))
            throw new OnlyNumbersAllowed();
    }

    class OnlyNumbersAllowed extends Throwable {

    }
}
