import java.util.HashSet;
import java.util.Set;

/*
An isogram is a word that has no repeating letters, consecutive or non-consecutive.
Implement a function that determines whether a string that contains only letters is an isogram.
Assume the empty string is an isogram. Ignore letter case.
 */
public class Isogram {

    public static boolean isIsogram(String string) {
        Set<Character> letters = new HashSet<Character>();

        for (int i = 0; i < string.length(); ++i) {
            if (letters.contains(string.toLowerCase().charAt(i))) {
                return false;
            }

            letters.add(string.charAt(i));
        }

        return true;
    }

}
