import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/*
Write a function that takes a string of braces, and determines if the order of the braces is valid. It should return true if the string is valid, and false if it's invalid.

This Kata is similar to the Valid Parentheses Kata, but introduces new characters: brackets [], and curly braces {}. Thanks to @arnedag for the idea!

All input strings will be nonempty, and will only consist of parentheses, brackets and curly braces: ()[]{}.
What is considered Valid?

A string of braces is considered valid if all braces are matched with the correct brace.
Examples

"(){}[]"   =>  True
"([{}])"   =>  True
"(}"       =>  False
"[(])"     =>  False
"[({})](]" =>  False


 */

public class BraceChecker {

    private final static List<Character> OPENING_BRACES = Arrays.asList('(', '[', '{');
    private final static List<Character> CLOSING_BRACES = Arrays.asList(')', ']', '}');


    public boolean isValid(String braces) {
        Stack<Character> braceStack = new Stack<>();
        for (int i = 0; i < braces.length(); i++) {
            char current = braces.charAt(i);
            if (OPENING_BRACES.contains(current)) {
                braceStack.push(current);
            }

            if (CLOSING_BRACES.contains(current)) {
                if (braceStack.isEmpty()) {
                    return false;
                }
                char lastBrace = braceStack.peek();
                if (isLegalClosingBrace(current, lastBrace)) {
                    braceStack.pop();
                } else {
                    return false;
                }
            }
        }

        return braceStack.isEmpty();
    }

    private boolean isLegalClosingBrace(char closing, char opening) {
        return closing == '}' && opening == '{' ||
                closing == ')' && opening == '(' ||
                closing == ']' && opening == '[';
    }
}
