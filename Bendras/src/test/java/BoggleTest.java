import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BoggleTest {

    private Boggle boggle;
    private char[][] emptyBoard;

    @Before
    public void setUp() {
        boggle = new Boggle();
        emptyBoard = new char[][]{};
    }

    @Test(expected = Boggle.InvalidDataException.class)
    public void shouldNotAllowNullBoard() {
        boggle.check(null, "ASD");
    }

    @Test(expected = Boggle.InvalidDataException.class)
    public void shouldNotAllowNullWord() {
        boggle.check(emptyBoard, null);
    }

    @Test
    public void shouldReturnTrueIfWordIsEmpty() {
        Assert.assertTrue(boggle.check(emptyBoard, ""));
    }

    @Test
    public void shouldTrimWhiteSpace() {
        Assert.assertTrue(boggle.check(emptyBoard, "   "));
    }

    @Test(expected = Boggle.BoardDimensionsShouldBeNxN.class)
    public void boggleBoardShouldHaveNxNDimensions() {
        boggle.check(new char[7][4], "ASD");
    }

    @Test
    public void boardLettersAndWordLettersAreComparedAsUppercase() {
        char[][] boardWithOneUppercaseLetter = new char[][]{{'A'}};
        String wordWithOneLowerCaseLetter = "a";
        Assert.assertTrue(boggle.check(boardWithOneUppercaseLetter, wordWithOneLowerCaseLetter));
    }

    @Test
    public void shouldSearchForLetterToTheLeft() {
        char[][] board = createAQNBBoard();
        String word = "AQ";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test
    public void shouldSearchForLetterToTheRight() {
        char[][] board = createAQNBBoard();
        String word = "QA";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test
    public void shouldSearchForLetterUp() {
        char[][] board = createAQNBBoard();
        String word = "NA";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test
    public void shouldSearchForLetterDown() {
        char[][] board = createAQNBBoard();
        String word = "AN";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test
    public void shouldSearchForLetterNorthEast() {
        char[][] board = createAQNBBoard();
        String word = "NQ";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test
    public void shouldSearchForLetterNorthWest() {
        char[][] board = createAQNBBoard();
        String word = "BA";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test
    public void shouldSearchForLetterSouthEast() {
        char[][] board = createAQNBBoard();
        String word = "AB";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test
    public void shouldSearchForLetterSouthWest() {
        char[][] board = createAQNBBoard();
        String word = "QN";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test(expected = Boggle.OnlyLettersInBoard.class)
    public void shouldOnlyAllowLettersAToZ_And_aToz_InBoard() {
        char[][] board = new char[][]{{'#', 'Q'},
                {'N', 'B'}};
        String word = "QN";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test(expected = Boggle.OnlyLettersInWord.class)
    public void shouldOnlyAllowLettersAToZ_And_aToz_InWord() {
        char[][] board = createAQNBBoard();
        String word = "#N";
        Assert.assertTrue(boggle.check(board, word));
    }

    @Test
    public void shouldFindWordBingo() {
        char[][] board = create4x4Board();
        String word = "BINGO";
        Assert.assertTrue(boggle.check(board, word));
    }

    private char[][] create4x4Board() {
        return new char[][]{{'I', 'L', 'A', 'W'},
                {'B', 'N', 'G', 'E'},
                {'I', 'U', 'A', 'O'},
                {'A', 'S', 'R', 'L'}};
    }

    @Test
    public void shouldNotFindWordSinus() {
        char[][] board = create4x4Board();
        String word = "SINUS";
        Assert.assertFalse(boggle.check(board, word));
    }

    private char[][] createAQNBBoard() {
        return new char[][]{{'A', 'Q'},
                {'N', 'B'}};
    }
}