import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FactorialTrailingZerosTest {

    private FactorialTrailingZeros trailingZeros;

    @Before
    public void setUp() {
        trailingZeros = new FactorialTrailingZeros();
    }

    @Test
    public void shouldReturnZero() throws FactorialTrailingZeros.NumberMustBeNonNegative {
        Assert.assertEquals(trailingZeros.zeros(1), 0);
        Assert.assertEquals(trailingZeros.zeros(2), 0);
        Assert.assertEquals(trailingZeros.zeros(3), 0);
        Assert.assertEquals(trailingZeros.zeros(4), 0);
    }

    @Test
    public void shouldReturnOne() throws FactorialTrailingZeros.NumberMustBeNonNegative {
        Assert.assertEquals(trailingZeros.zeros(5), 1);
    }

    @Test
    public void shouldReturn1151() throws FactorialTrailingZeros.NumberMustBeNonNegative {
        Assert.assertEquals(trailingZeros.zeros(4617), 1151);
    }

    @Test(expected = FactorialTrailingZeros.NumberMustBeNonNegative.class)
    public void numberMustBeNonNegative() throws FactorialTrailingZeros.NumberMustBeNonNegative {
        trailingZeros.zeros(-1);
        trailingZeros.zeros(-2);
        trailingZeros.zeros(-3);
    }
}