import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FinderTest {

    private Finder finder;

    @Before
    public void setUp() {
        finder = new Finder();
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowNullInput() throws Finder.IllegalSymbolsInInput, Finder.InvalidMatrixDimensions {
        finder.pathFinder(null);
    }

    @Test(expected = Finder.IllegalSymbolsInInput.class)
    public void shouldNotAllowIllegalSymbolsInMaze() throws Finder.IllegalSymbolsInInput, Finder.InvalidMatrixDimensions {
        String maze = "w.o.\n" +
                "W...";
        finder.pathFinder(maze);
    }

    @Test(expected = Finder.InvalidMatrixDimensions.class)
    public void shouldNotAllowInvalidMatrixDimensions() throws Finder.InvalidMatrixDimensions, Finder.IllegalSymbolsInInput {
        String maze = "W...\n" +
                      "W....\n" +
                      ".W..";
        finder.pathFinder(maze);
    }

    @Test
    public void shouldNotFindPathInStraightBlockedPath() throws Finder.IllegalSymbolsInInput, Finder.InvalidMatrixDimensions {
        String maze = ".w.";
        Assert.assertEquals(-1, finder.pathFinder(maze));
    }

    @Test
    public void shouldFindStraightPath() throws Finder.IllegalSymbolsInInput, Finder.InvalidMatrixDimensions {
        String maze = "...";
        Assert.assertEquals(3, finder.pathFinder(maze));
    }
}