import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PrinterTest {

    private Printer printer;

    @BeforeEach
    void setUp() {
        printer = new Printer();
    }

    @Test
    void givenEmptyControlString_ShouldThrowEmptyControlStringException() {
        assertThrows(Printer.EmptyControlStringException.class,
                () -> printer.printError(""));
    }

    @Test
    void givenNullControlString_ShouldThrowNullPointerException() {
        assertThrows(NullPointerException.class,
                () -> printer.printError(null));
    }

    @Test
    void shouldOnlyAllowControlStringWithLowerCaseLettersBetweenA_Z() {
        assertThrows(Printer.IllegalSymbolsInControlString.class,
                () -> printer.printError("a7"));
    }

    @Test
    void givenControlString_OfLengthOne_WithOneDetectedPrinterProblem_ShouldReturn_OneOutOfOne() {
        assertEquals("1/1", printer.printError("z"));
    }

    @Test
    void givenControlString_OfLengthTen_WithSixDetectedPrinterProblems_ShouldReturn_SixOutOfTen() {
        assertEquals("6/10", printer.printError("neeopzvlmn"));
    }
}