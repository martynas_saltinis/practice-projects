import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleLinearTest {

    private DoubleLinear doubleLinear;

    @Before
    public void setUp() {
        doubleLinear = new DoubleLinear();
    }

    @Test(expected = DoubleLinear.NegativeIndexNotAllowed.class)
    public void shouldNotAllowNegativeIndex() throws DoubleLinear.NegativeIndexNotAllowed {
        doubleLinear.dblLinear(-1);
    }

    @Test
    public void firstNumberShouldBeOne() throws DoubleLinear.NegativeIndexNotAllowed {
        Assert.assertEquals(doubleLinear.dblLinear(0),1);
    }

    @Test
    public void secondNumberShouldBeThree() throws DoubleLinear.NegativeIndexNotAllowed {
        Assert.assertEquals(doubleLinear.dblLinear(1),3);
    }

    @Test
    public void thirdNumberShouldBeFour() throws DoubleLinear.NegativeIndexNotAllowed {
        Assert.assertEquals(doubleLinear.dblLinear(2),4);
    }

    @Test
    public void fifthNumberShouldBeNine_Sorted() throws DoubleLinear.NegativeIndexNotAllowed {
        Assert.assertEquals(doubleLinear.dblLinear(4),9);
    }
}