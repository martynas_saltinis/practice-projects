import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GreenNumbersTest {

    private GreenNumbers greenNumbers;

    @BeforeEach
    void setUp() {
        greenNumbers = new GreenNumbers();
    }

    @Test
    void givenInputLowerThanOne_ShouldThrowInputLowerThanOneException() {
        assertThrows(GreenNumbers.InputLowerThanOneException.class,
                () -> greenNumbers.get(0));
        assertThrows(GreenNumbers.InputLowerThanOneException.class,
                () -> greenNumbers.get(-5));
    }

    @Test
    void givenOne_ShouldReturnOne() {
        assertEquals(BigInteger.valueOf(1), greenNumbers.get(1));
    }


    @Test
    void givenTwo_ShouldReturnFive() {
        assertEquals(BigInteger.valueOf(5), greenNumbers.get(2));
    }

    @Test
    void shouldSolveOneHundred() {
        assertEquals(new BigInteger("6188999442576576769103890995893380022607743740081787109376"), greenNumbers.get(100));
    }
}