import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ObservedPinTest {

    private ObservedPin observedPin;

    @Before
    public void setUp() {
        observedPin = new ObservedPin();
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowNullInput() throws ObservedPin.OnlyNumbersAllowed {
        observedPin.getPINs(null);
    }

    @Test(expected = ObservedPin.OnlyNumbersAllowed.class)
    public void shouldOnlyAllowNumbers() throws ObservedPin.OnlyNumbersAllowed {
        observedPin.getPINs("as,+d88");
    }

    @Test(expected = ObservedPin.OnlyNumbersAllowed.class)
    public void shouldRequireNonEmptyPin() throws ObservedPin.OnlyNumbersAllowed {
        observedPin.getPINs("");
    }

    @Test
    public void shouldReturnOnlyHorizontallyAndVerticallyAdjacentDigits() throws ObservedPin.OnlyNumbersAllowed {
        String[] expectationsForOne = {"1", "2", "4"};
        String[] expectationsForEight = {"5", "2", "4", "6", "8"};
        String[] expectationsForZero = {"0", "8"};
        test("1", observedPin.getPINs("1"), Arrays.asList(expectationsForOne));
        test("5", observedPin.getPINs("5"), Arrays.asList(expectationsForEight));
        test("0", observedPin.getPINs("0"), Arrays.asList(expectationsForZero));
    }

    @Test
    public void shouldWorkOnMultipleDigitPins() throws ObservedPin.OnlyNumbersAllowed {
        String[] expectations10 = {"10", "18", "20", "28", "40", "48"};
        String[] expectations11 = {"11", "21", "41", "12", "22", "42", "14", "24", "44"};
        String[] expectations369 = {"236", "238", "239", "256", "258", "259", "266", "268", "269", "296", "298", "299", "336", "338", "339", "356", "358", "359", "366", "368", "369", "396", "398", "399", "636", "638", "639", "656", "658", "659", "666", "668", "669", "696", "698", "699"};

        test("10", observedPin.getPINs("10"), Arrays.asList(expectations10));
        test("11", observedPin.getPINs("11"), Arrays.asList(expectations11));
        test("369", observedPin.getPINs("369"), Arrays.asList(expectations369));


    }

    private final static Comparator<String> comp = Comparator.naturalOrder();

    private void test(String entered, List<String> expected, List<String> result) {
        result.sort(comp);
        expected.sort(comp);
        Assert.assertEquals("For observed PIN " + entered, result, expected);
    }

}