import org.junit.Test;

import static org.junit.Assert.*;

public class BiggestNumberTest {

    private BiggestNumber biggestNumber;

    public BiggestNumberTest() {
        biggestNumber = new BiggestNumber();
    }

    @Test(expected = BiggestNumber.OnlyPositiveWholeNumbersAllowed.class)
    public void shouldOnlyAllowPositiveIntegers() {
        biggestNumber.nextBiggerNumber(-1);
    }

    @Test
    public void givenNumberWithOneDigit_ShouldReturnDigit() {
        assertEquals(9, biggestNumber.nextBiggerNumber(9));
        assertEquals(3, biggestNumber.nextBiggerNumber(3));
    }

    @Test
    public void givenTwelve_ShouldReturnTwentyOne() {
        assertEquals(21, biggestNumber.nextBiggerNumber(12));
    }

    @Test
    public void givenTwentyOne_ShouldReturnTwentyOne() {
        assertEquals(21, biggestNumber.nextBiggerNumber(21));
    }

    @Test
    public void givenTwoHundredFiftyOne_ShouldReturnFiveHundredTwentyOne() {
        assertEquals(521, biggestNumber.nextBiggerNumber(251));
    }
}