import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SecretDetectiveTest {

    private SecretDetective secretDetective;

    @Before
    public void setUp() {
        secretDetective = new SecretDetective();
    }

    @Test(expected = NullPointerException.class)
    public void givenTripletsMustNotBeNull() throws SecretDetective.DuplicatesNotAllowed, SecretDetective.TripletsMustBeOfSizeThree {
        secretDetective.recoverSecret(null);
    }

    @Test(expected = SecretDetective.TripletsMustBeOfSizeThree.class)
    public void givenTripletsMustOfSizeThree() throws SecretDetective.DuplicatesNotAllowed, SecretDetective.TripletsMustBeOfSizeThree {
        char[][] triplets = {
                {'c','a'},
                {'a','a', 't'},
        };
        secretDetective.recoverSecret(triplets);
    }

    @Test
    public void shouldGuessWordCatFromOneTriplet() throws SecretDetective.DuplicatesNotAllowed, SecretDetective.TripletsMustBeOfSizeThree {
        char[][] triplets = {
                {'c','a','t'}
        };
        assertEquals("cat", secretDetective.recoverSecret(triplets));
    }

    @Test(expected = SecretDetective.DuplicatesNotAllowed.class)
    public void shouldNotAllowDuplicateLetters() throws SecretDetective.DuplicatesNotAllowed, SecretDetective.TripletsMustBeOfSizeThree {
        char[][] triplets = {
                {'o','o','t'},
                {'b','o','t'}
        };
        secretDetective.recoverSecret(triplets);
    }

    @Test
    public void shouldGuessWordNiceFromTwoTriplets() throws SecretDetective.DuplicatesNotAllowed, SecretDetective.TripletsMustBeOfSizeThree {
        char[][] triplets = {
                {'n','i','c'},
                {'i','c','e'}
        };
        assertEquals("nice", secretDetective.recoverSecret(triplets));
    }

    @Test
    public void shouldGuessWordNiceFromThreeTriplets() throws SecretDetective.DuplicatesNotAllowed, SecretDetective.TripletsMustBeOfSizeThree {
        char[][] triplets = {
                {'n','i','e'},
                {'i','c','e'},
                {'n','c','e'}
        };
        assertEquals("nice", secretDetective.recoverSecret(triplets));
    }

    @Test
    public void shouldGuessWhatisup() throws SecretDetective.DuplicatesNotAllowed, SecretDetective.TripletsMustBeOfSizeThree {
        char[][] triplets = {
                {'t','u','p'},
                {'w','h','i'},
                {'t','s','u'},
                {'a','t','s'},
                {'h','a','p'},
                {'t','i','s'},
                {'w','h','s'}
        };
        assertEquals("whatisup", secretDetective.recoverSecret(triplets));
    }
}