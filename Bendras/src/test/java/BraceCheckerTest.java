import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BraceCheckerTest {

    private BraceChecker braceChecker;

    @Before
    public void setUp() {
        braceChecker = new BraceChecker();
    }

    @Test
    public void shouldReturnValidParentheses() {
        assertTrue(braceChecker.isValid("()"));
    }

    @Test
    public void shouldReturnValidBraces() {
        assertTrue(braceChecker.isValid("({})"));
    }

    @Test
    public void shouldReturnInValidBraces() {
        assertFalse(braceChecker.isValid("{(})"));
    }

    @Test
    public void shouldReturnValidBraces_EmptyString() {
        assertTrue(braceChecker.isValid(""));
    }
}