import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AlternatingSplitTest {

    private AlternatingSplit alternatingSplit;

    @Before
    public void setUp() {
        alternatingSplit = new AlternatingSplit();
    }

    @Test
    public void shouldReturnNullWhenStringIsNull() {
        assertNull(alternatingSplit.encrypt(null, 1));
        assertNull(alternatingSplit.decrypt(null, 1));
    }

    @Test
    public void shouldReturnEmptyWhenStringIsEmpty() {
        assertEquals(alternatingSplit.encrypt("", 1), "");
        assertEquals(alternatingSplit.decrypt("", 1), "");
    }

    @Test
    public void shouldReturnUnchangedStringWhenNIsZero() {
        assertEquals(alternatingSplit.encrypt("asd", 0), "asd");
        assertEquals(alternatingSplit.decrypt("asd", 0), "asd");
    }

    @Test
    public void shouldEncrypt() {
        assertEquals(alternatingSplit.encrypt("This is a test!", 1),"hsi  etTi sats!");
        assertEquals(alternatingSplit.encrypt("This is a test!", 2),"s eT ashi tist!");
        assertEquals(alternatingSplit.encrypt("This is a test!", 3)," Tah itse sits!");
        assertEquals(alternatingSplit.encrypt("This is a test!", 4),"This is a test!");
    }

    @Test
    public void shouldDecrypt() {
        assertEquals(alternatingSplit.decrypt("hsi  etTi sats!", 1),"This is a test!");
        assertEquals(alternatingSplit.decrypt("s eT ashi tist!", 2),"This is a test!");
        assertEquals(alternatingSplit.decrypt(" Tah itse sits!", 3),"This is a test!");
        assertEquals(alternatingSplit.decrypt("This is a test!", 4),"This is a test!");
    }
}
