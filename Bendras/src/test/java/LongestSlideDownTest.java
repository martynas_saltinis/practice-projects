import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LongestSlideDownTest {

    private LongestSlideDown longestSlideDown;

    @Before
    public void setUp() {
        longestSlideDown = new LongestSlideDown();
    }

    @Test(expected = LongestSlideDown.PyramidMustHaveLevels.class)
    public void shouldThrowPyramidMustHaveLevels() throws LongestSlideDown.PyramidMustHaveLevels, LongestSlideDown.PyramidMustNotBeNull {
        int[][] pyramid = { };
        longestSlideDown.longestSlideDown(pyramid);
    }

    @Test(expected = LongestSlideDown.PyramidMustNotBeNull.class)
    public void shouldThrowPyramidMustNotBeNull() throws LongestSlideDown.PyramidMustNotBeNull, LongestSlideDown.PyramidMustHaveLevels {
        int[][] pyramid = null;
        longestSlideDown.longestSlideDown(pyramid);
    }

    @Test
    public void shouldReturnReturn3() throws LongestSlideDown.PyramidMustHaveLevels, LongestSlideDown.PyramidMustNotBeNull {
        int[][] pyramid = { {3, 1} };
        Assert.assertEquals(longestSlideDown.longestSlideDown(pyramid), 3);
    }

    @Test
    public void shouldReturn3Plus5() throws LongestSlideDown.PyramidMustHaveLevels, LongestSlideDown.PyramidMustNotBeNull {
        int[][] pyramid = {
                {3, 1},
                {2, 5, 1}};
        Assert.assertEquals(longestSlideDown.longestSlideDown(pyramid), 8);
    }

    @Test
    public void shouldReturnLeftSidePath() throws LongestSlideDown.PyramidMustHaveLevels, LongestSlideDown.PyramidMustNotBeNull {
        int[][] pyramid = {
                {6, 1},
                {2, 1, 4}};
        Assert.assertEquals(longestSlideDown.longestSlideDown(pyramid), 8);
    }

    @Test
    public void shouldReturn1() throws LongestSlideDown.PyramidMustHaveLevels, LongestSlideDown.PyramidMustNotBeNull {
        int[][] pyramid = {
                {1},
                };
        Assert.assertEquals(longestSlideDown.longestSlideDown(pyramid), 1);
    }

    @Test
    public void shouldReturn75Plus64Plus82() throws LongestSlideDown.PyramidMustHaveLevels, LongestSlideDown.PyramidMustNotBeNull {
        int[][] pyramid = new int[][]{
                {75},
                {95, 64},
                {17, 47, 82},
        };
        Assert.assertEquals(longestSlideDown.longestSlideDown(pyramid), 221);
    }

    @Test
    public void shouldReturn1074() throws LongestSlideDown.PyramidMustHaveLevels, LongestSlideDown.PyramidMustNotBeNull {
        int[][] pyramid = new int[][]{
                {75},
                {95, 64},
                {17, 47, 82},
                {18, 35, 87, 10},
                {20, 4, 82, 47, 65},
                {19, 1, 23, 75, 3, 34},
                {88, 2, 77, 73, 7, 63, 67},
                {99, 65, 4, 28, 6, 16, 70, 92},
                {41, 41, 26, 56, 83, 40, 80, 70, 33},
                {41, 48, 72, 33, 47, 32, 37, 16, 94, 29},
                {53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14},
                {70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57},
                {91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48},
                {63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31},
                {4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23},
        };
        Assert.assertEquals(longestSlideDown.longestSlideDown(pyramid), 1074);
    }
}