import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EnoughIsEnoughTest {

    private EnoughIsEnough enough;
    private final int[] EMPTY_ARRAY = {};

    @Before
    public void setup() {
        enough = new EnoughIsEnough();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionOnNull() {
        enough.deleteNth(null, 0);
    }

    @Test
    public void shouldReturnEmptyArray_FromEmptyArray() {
        int[] elements = {};
        Assert.assertArrayEquals(enough.deleteNth(elements, 0),EMPTY_ARRAY);
    }

    @Test
    public void shouldReturnEmptyArray_FromEmptyArray_FiveOccurrences() {
        int[] elements = {};
        Assert.assertArrayEquals(enough.deleteNth(elements, 5),EMPTY_ARRAY);
    }

    @Test
    public void shouldReturnEmptyArray_ZeroOccurrences_PopulatedInput() {
        int[] elements = {1,2,3,4,5,1,2,3,4,5};
        Assert.assertArrayEquals(enough.deleteNth(elements, 0), EMPTY_ARRAY);
    }

    @Test
    public void shouldReturnOneTwoThreeFourFive_OnceOccurrence_PopulatedInput() {
        int[] elements = {1,2,3,4,5,1,2,3,4,5};
        Assert.assertArrayEquals(enough.deleteNth(elements, 1), new int[]{1,2,3,4,5});
    }

    @Test
    public void shouldReturnFiveOnes() {
        int[] elements = {1,1,1,1,1};
        Assert.assertArrayEquals(enough.deleteNth(elements, 5), new int[]{1,1,1,1,1});
    }

}
