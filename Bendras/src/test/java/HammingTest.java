import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HammingTest {

    private Hamming hamming;

    @Before
    public void setUp() {
        hamming = new Hamming();

    }

    @Test(expected = Hamming.InputMustBeBiggerThanZero.class)
    public void inputShouldBeIntegerHigherThanZero() throws Hamming.InputMustBeBiggerThanZero {
        hamming.hamming(0);
        hamming.hamming(-5);
    }

    @Test
    public void firstHammingNumberShouldBeOne() throws Hamming.InputMustBeBiggerThanZero {
        Assert.assertEquals((long)1, hamming.hamming(1));
    }

    @Test
    public void secondHammingNumberShouldBeTwo() throws Hamming.InputMustBeBiggerThanZero {
        Assert.assertEquals((long)2, hamming.hamming(2));
    }

    @Test
    public void tenthHammingNumberShouldBeTwelve() throws Hamming.InputMustBeBiggerThanZero {
        Assert.assertEquals((long)12, hamming.hamming(10));
    }
}