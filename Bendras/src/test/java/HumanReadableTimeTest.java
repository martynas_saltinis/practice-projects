import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HumanReadableTimeTest {

    private HumanReadableTime timeFormatter;

    @Before
    public void setUp() {
        timeFormatter = new HumanReadableTime();
    }

    @Test(expected = HumanReadableTime.OnlyPositiveNumbersAllowed.class)
    public void shouldAcceptOnlyPositiveNumbers() throws HumanReadableTime.OnlyPositiveNumbersAllowed {
        timeFormatter.makeReadable(-5);
    }

    @Test
    public void shouldFormatOneSecond() throws HumanReadableTime.OnlyPositiveNumbersAllowed {
        Assert.assertEquals(timeFormatter.makeReadable(1), "00:00:01");
    }

    @Test
    public void shouldFormatTenSeconds() throws HumanReadableTime.OnlyPositiveNumbersAllowed {
        Assert.assertEquals(timeFormatter.makeReadable(10), "00:00:10");
    }

    @Test
    public void shouldFormatOneMinute() throws HumanReadableTime.OnlyPositiveNumbersAllowed {
        Assert.assertEquals(timeFormatter.makeReadable(60), "00:01:00");
    }

    @Test
    public void shouldFormatOneHourOneMinuteOneSecond() throws HumanReadableTime.OnlyPositiveNumbersAllowed {
        Assert.assertEquals(timeFormatter.makeReadable(3661), "01:01:01");
    }

    @Test
    public void shouldFormat99Hours_59Minutes_59Seconds() throws HumanReadableTime.OnlyPositiveNumbersAllowed {
        Assert.assertEquals(timeFormatter.makeReadable(359999), "99:59:59");
    }

    @Test
    public void shouldFormat100Hours() throws HumanReadableTime.OnlyPositiveNumbersAllowed {
        Assert.assertEquals(timeFormatter.makeReadable(360000), "100:00:00");
    }
}