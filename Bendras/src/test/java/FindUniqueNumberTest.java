import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FindUniqueNumberTest {

    FindUniqueNumber findUniqueNumber;
    private final double PRECISION = 0.0000000000001;


    @Before
    public void setUp() {
        findUniqueNumber = new FindUniqueNumber();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerException_NullInput() {
        findUniqueNumber.findUnique(null);
    }

    @Test
    public void shouldReturnNull_NoUniqueElement() {
        int[] elements = new int[]{1,1,1,1};
        Assert.assertEquals(findUniqueNumber.findUnique(elements), null);
    }

    @Test
    public void shouldReturnOne_OneElement() {
        int[] elements = new int[]{1};
        Assert.assertEquals(findUniqueNumber.findUnique(elements), 1.0, PRECISION);

    }

    @Test
    public void shouldReturnNull_ThreeElementsDoubleElements() {
        int[] elements = new int[]{2,1,3,3,1,2};
        Assert.assertEquals(findUniqueNumber.findUnique(elements), null);

    }

    @Test
    public void shouldReturnOne_TwoRepeatingOneUniqueElements(){
        int[] elements = new int[]{2,3,3,1,2};
        Assert.assertEquals(findUniqueNumber.findUnique(elements), 1, PRECISION);
    }
}
