import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StripCommentsTest {

    private StripComments stripComments;

    @Before
    public void setUp() {
        stripComments = new StripComments();
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowNullText() {
        stripComments.stripComments(null, new String[]{"#"});
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowNullCommentSymbols() {
        stripComments.stripComments("asd", null);
    }

    @Test
    public void shouldReturnWholeTextIfThereAreNoCommentSymbols() {
        Assert.assertEquals("asd", stripComments.stripComments("asd", new String[]{}));
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowOneOfTheCommentSymbolsToBeNull() {
        stripComments.stripComments("asd", new String[]{"#", null});
    }

    @Test
    public void shouldDeleteEverythingAfterCommentSymbolInOneLine() {
        Assert.assertEquals("asd", stripComments.stripComments("asd#dqw", new String[]{"#"}));
    }

    @Test
    public void shouldDeleteEverythingInLineIfEmptyCommentSymbolIsPassedIn() {
        String text = "asddqw#";
        Assert.assertEquals("", stripComments.stripComments(text, new String[]{"", "#"}));
        Assert.assertEquals("", stripComments.stripComments(text, new String[]{"#", ""}));
        Assert.assertEquals("", stripComments.stripComments(text, new String[]{""}));
    }

    @Test
    public void shouldDeleteWhiteSpaceAtTheEndOfTheLine() {
        String text = "asddqw   #io,";
        Assert.assertEquals("asddqw", stripComments.stripComments(text, new String[]{"#"}));
    }

    @Test
    public void shouldReturnTwoWholeLines() {
        String text = "first#wqdqqw\nsecond#1984qwd";
        Assert.assertEquals("first\nsecond", stripComments.stripComments(text, new String[]{"#"}));
    }

    @Test
    public void shouldNotDeleteTextAfterNewLineIfNewLineIsInCommentSymbols() {
        String text = "first\nsecond";
        Assert.assertEquals("first\nsecond", stripComments.stripComments(text, new String[]{"\n"}));
    }
}