import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IsogramsTest {

    @Test
    public void shouldDetermineNotIsogram_OnStringBoot() {
        Assert.assertEquals(Isogram.isIsogram("Boot"), false);
    }

    @Test
    public void shouldDetermineIsogram_OnStringCat() {
        Assert.assertEquals(Isogram.isIsogram("cat"), true);
    }

    @Test
    public void shouldDetermineIsogram_OnStringDermatoglyphics() {
        Assert.assertEquals(Isogram.isIsogram("Dermatoglyphics"), true);
    }

    @Test
    public void shouldDetermineNotIsogram_OnStringWithSameUppercaseAndLowercaseLetters() {

        Assert.assertEquals(false, Isogram.isIsogram("moOse"));

    }

    @Test
    public void shouldDetermineIsogram_OnEmptyString() {

        Assert.assertEquals(false, Isogram.isIsogram("moOse"));

    }
}
