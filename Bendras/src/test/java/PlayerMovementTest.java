import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class PlayerMovementTest {

    private static Map<Integer, String> keysDir = new HashMap<>();

    static {
        keysDir.put(8, "Up");
        keysDir.put(2, "Down");
        keysDir.put(4, "Left");
        keysDir.put(6, "Right");
    }

    private PlayerMovement player;
    private Input input;


    @Before
    public void setUp() {
        input = new Input();
        player = new PlayerMovement(0, 0, input);
    }


    @Test
    public void shouldCreateTwoEqualTiles() {
        Tile tile = new Tile(1, 1);
        Tile other = new Tile(1, 1);
        Assert.assertEquals(tile, other);
    }

    @Test
    public void shouldCreateTwoDifferentTiles() {
        Tile tile = new Tile(1, 1);
        Tile other = new Tile(1, 2);
        Assert.assertNotEquals(tile, other);
        Assert.assertNotEquals(tile, null);
    }

    @Test
    public void shouldAllowToGetPosition() {
        Assert.assertEquals(new Tile(0, 0), player.getPosition());
    }

    @Test
    public void initialPlayerDirectionShouldBeUp_8() {
        Assert.assertEquals("Up", keysDir.get(player.getDirection()));
    }

    @Test
    public void inputShouldHoldListOfPressedDirections() {
        input.press(8);
        input.press(2);
        assertOnlyTrueUpAndDown(input);

        input.release(2);
        assertOnlyTrueUp(input);

        input.clear();
        assertAllFalse(input);
    }

    @Test
    public void whenDifferentDirectionIsPressed_PlayerShouldOnlyChangeDirectionAndNotMove() {
        input.press(2);

        player.update();

        int x = 0;
        int y = 0;
        int dir = 2;
        assertPositionAndDirection(x, y, dir);
    }

    private void assertPositionAndDirection(int x, int y, int dir) {
        Assert.assertEquals(new Tile(x, y), player.getPosition());
        Assert.assertEquals(dir, player.getDirection());
    }

    @Test
    public void whenSameDirectionIsPressed_PlayerShouldMoveInThatDirection() {
        input.press(8);

        player.update();

        assertPositionAndDirection(0, 1, 8);
    }

    @Test
    public void whenAllKeysAreReleased_PlayerWillStandStillAndNotChangeDirection() {
        player.update();
        assertPositionAndDirection(0, 0, 8);
    }

    @Test
    public void whenCurrentDirectionIsReleased_PlayerShouldStandStill() {
        input.press(8);
        player.update();

        assertPositionAndDirection(0, 1, 8);

        input.release(8);
        player.update();

        assertPositionAndDirection(0, 1, 8);
    }

    @Test
    public void whenANewDirectionIsPressed_ItWillGainPrecedenceOverPreviousDirection() {
        input.press(2);
        player.update();
        player.update();

        assertPositionAndDirection(0, -1, 2);

        input.press(8);
        player.update();

        assertPositionAndDirection(0, -1, 8);
    }

    @Test
    public void whenMultipleDirectionsArePressed_PriorityWillBeDescending_UpDownLeftRight() {
        input.press(4);
        input.press(6);
        input.press(8);
        input.press(2);
        player.update();

        assertPositionAndDirection(0, 1, 8);

        input.release(8);
        player.update();

        assertPositionAndDirection(0, 1, 2);

        input.release(2);
        player.update();

        assertPositionAndDirection(0, 1, 4);

        input.release(4);
        player.update();

        assertPositionAndDirection(0, 1, 6);
    }

    private void assertAllFalse(Input input) {
        Assert.assertFalse(input.getState(2));
        Assert.assertFalse(input.getState(8));
        Assert.assertFalse(input.getState(4));
        Assert.assertFalse(input.getState(6));
    }

    private void assertOnlyTrueUp(Input input) {
        Assert.assertFalse(input.getState(2));
        Assert.assertTrue(input.getState(8));
        Assert.assertFalse(input.getState(4));
        Assert.assertFalse(input.getState(6));
    }

    private void assertOnlyTrueUpAndDown(Input input) {
        Assert.assertTrue(input.getState(2));
        Assert.assertTrue(input.getState(8));
        Assert.assertFalse(input.getState(4));
        Assert.assertFalse(input.getState(6));
    }



}